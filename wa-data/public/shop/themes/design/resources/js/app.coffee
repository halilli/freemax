(($) ->

  $.fn.slider = ->
    $ul = @find('ul')
    $li = $ul.find('li')
    count_slider = $li.length
    width_box = 100 * count_slider
    width_slider = 100 / count_slider * 0.4
    width_current = 100 / count_slider * 0.6
    current_slider = 0
    $ul.width width_box + '%'
    $li.width width_slider + '%'
    $li.filter('.current').width width_current + '%'
    $ul.css 'transition', '400ms'
    $li.delay(200).queue (next)->
      $(this).css 'transition', 'width 400ms, opacity 400ms'
      next()
      return

    selectSlider = (n) ->
      if n < 0
        n = 0
      if n > count_slider
        n = count_slider
      current_slider = n
      updateSlider()
      return

    nextSlider = ->
      current_slider++
      if current_slider >= count_slider
        current_slider = 0
      updateSlider()
      return

    prevSlider = ->
      current_slider--
      if current_slider < 0
        current_slider = count_slider - 1
      updateSlider()
      return

    updateSlider = ->
      $li.removeClass 'current'
      $($li[current_slider]).addClass 'current'

      $li.width width_slider + '%'
      $li.filter('.current').width width_current + '%'

      $dots.removeClass 'current'
      $($dots[current_slider]).addClass 'current'

      $ul.css 'transform', 'translate3d(-' + current_slider * width_slider + '%, 0px, 0px)'
      return

    dots_tpl = ''
    $li.each (i) ->
      dots_tpl += '<div class="dot' + (if i == 0 then ' current' else '') + '" data-item="' + i + '"></div>'
      return
    # @append '<span class="prev"></span><span class="next"></span>'
    @append '<div class="dots">' + dots_tpl + '</div>'
    $dots = @find('.dots .dot')
    # @find('.next').click ->
    #   nextSlider()
    #   return
    # @find('.prev').click ->
    #   prevSlider()
    #   return
    @find('.dots .dot').click ->
      n = parseInt($(this).data('item'))
      selectSlider n
      return
    @find('ul li').click ->
      if $(this).hasClass('current')
        return
      n = parseInt($(this).data('item'))
      selectSlider n
      return
    return
  return
) jQuery
