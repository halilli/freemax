(function($) {
  $.fn.slider = function() {
    var $dots, $li, $ul, count_slider, current_slider, dots_tpl, nextSlider, prevSlider, selectSlider, updateSlider, width_box, width_current, width_slider;
    $ul = this.find('ul');
    $li = $ul.find('li');
    count_slider = $li.length;
    width_box = 100 * count_slider;
    width_slider = 100 / count_slider * 0.4;
    width_current = 100 / count_slider * 0.6;
    current_slider = 0;
    $ul.width(width_box + '%');
    $li.width(width_slider + '%');
    $li.filter('.current').width(width_current + '%');
    $ul.css('transition', '400ms');
    $li.delay(200).queue(function(next) {
      $(this).css('transition', 'width 400ms, opacity 400ms');
      next();
    });
    selectSlider = function(n) {
      if (n < 0) {
        n = 0;
      }
      if (n > count_slider) {
        n = count_slider;
      }
      current_slider = n;
      updateSlider();
    };
    nextSlider = function() {
      current_slider++;
      if (current_slider >= count_slider) {
        current_slider = 0;
      }
      updateSlider();
    };
    prevSlider = function() {
      current_slider--;
      if (current_slider < 0) {
        current_slider = count_slider - 1;
      }
      updateSlider();
    };
    updateSlider = function() {
      $li.removeClass('current');
      $($li[current_slider]).addClass('current');
      $li.width(width_slider + '%');
      $li.filter('.current').width(width_current + '%');
      $dots.removeClass('current');
      $($dots[current_slider]).addClass('current');
      $ul.css('transform', 'translate3d(-' + current_slider * width_slider + '%, 0px, 0px)');
    };
    dots_tpl = '';
    $li.each(function(i) {
      dots_tpl += '<div class="dot' + (i === 0 ? ' current' : '') + '" data-item="' + i + '"></div>';
    });
    // @append '<span class="prev"></span><span class="next"></span>'
    this.append('<div class="dots">' + dots_tpl + '</div>');
    $dots = this.find('.dots .dot');
    // @find('.next').click ->
    //   nextSlider()
    //   return
    // @find('.prev').click ->
    //   prevSlider()
    //   return
    this.find('.dots .dot').click(function() {
      var n;
      n = parseInt($(this).data('item'));
      selectSlider(n);
    });
    this.find('ul li').click(function() {
      var n;
      if ($(this).hasClass('current')) {
        return;
      }
      n = parseInt($(this).data('item'));
      selectSlider(n);
    });
  };
})(jQuery);
