( function($) {

    var Reviews = ( function($) {

        Reviews = function(options) {
            var that = this;

            // DOM
            that.$wrapper = options["$wrapper"];
            that.$form = that.$wrapper.find(".s-form-wrapper form");

            // VARS

            // DYNAMIC VARS

            // INIT
            that.initClass();
        };

        Reviews.prototype.initClass = function() {
            var that = this;

            that.initSetRating();

            that.initAuthAdapters();

            that.initSubmit();
        };

        Reviews.prototype.initSetRating = function() {
            var that = this;

            that.$form.on("click", ".s-rates-wrapper .s-rate-item", function(event) {
                event.preventDefault();
                setRating($(this));
            });

            function setRating($link) {
                var $wrapper = $link.closest(".s-rates-wrapper"),
                    $links = $wrapper.find(".s-rate-item"),
                    $input = $wrapper.find("input[name=\"rate\"]"),
                    rate_count = $link.data("rate-count"),
                    empty_rate_class = "star-empty",
                    full_rate_class = "star";

                if (rate_count && rate_count > 0) {
                    $links
                        .removeClass(full_rate_class)
                        .addClass(empty_rate_class);

                    for ( var i = 0; i < rate_count; i++ ) {
                        $($links[i])
                            .removeClass(empty_rate_class)
                            .addClass(full_rate_class);
                    }

                    // SET FIELD VALUE
                    $input.val(rate_count);
                }
            }
        };

        Reviews.prototype.initAuthAdapters = function() {
            var that = this,
                $form = that.$form,
                $captcha = that.$wrapper.find(".wa-captcha"),
                $provider = that.$wrapper.find("#user-auth-provider"),
                current_provider = $provider.find(".selected").attr('data-provider');

            showCaptcha();

            function showCaptcha() {
                if (current_provider === 'guest' || !current_provider) {
                    $captcha.show();
                } else {
                    $captcha.hide();
                }
            }
        };

        Reviews.prototype.initSubmit = function() {
            var that = this,
                $form = that.$form,
                is_locked = false;

            $form.on("submit", function(event) {
                event.preventDefault();
                addReview();
            });

            function addReview() {
                if (!is_locked) {
                    is_locked = true;

                    var href = location.origin + "" + location.pathname.replace(/\/#\/[^#]*|\/#|\/$/g, '') + '/add/',
                        data = $form.serialize();

                    $.post(href, data, function(response) {
                        if (response.status === "fail") {
                            showErrors(response.errors);
                            $(".wa-captcha-img").trigger("click");
                        } else {
                            // Refresh without hash
                            location.href = location.pathname;
                        }
                    }, "json").always( function() {
                        is_locked = false;
                    });
                }
            }

            function showErrors(errors) {
                var $wrapper = $form.find(".s-errors-wrapper");

                // Clear old errors
                $wrapper.addClass("is-shown").html("");

                $.each(errors, function(name, text) {
                    $("<div class=\"error\" />").text(text).appendTo($wrapper);
                });
            }
        };

        return Reviews;

    })(jQuery);

    window.waTheme.init.shop.Reviews = Reviews;

})(jQuery);

